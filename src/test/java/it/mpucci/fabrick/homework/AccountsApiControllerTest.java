package it.mpucci.fabrick.homework;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Calendar;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.mpucci.fabrick.homework.api.AccountsApiController;
import it.mpucci.fabrick.homework.model.MoneyTransferRequest;

@AutoConfigureWebTestClient(timeout = "36000")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AccountsApiControllerTest {

	@Autowired
	private AccountsApiController controller;
	
	@Autowired
	private WebTestClient client;

	private static final String ACCOUNT_ID= "14537780";
	private static final String FROM_ACCOUNTING_DATE="2019-01-01";
	private static final String TO_ACCOUNTING_DATE="2019-12-01";
	private static final TimeZone tz = Calendar.getInstance().getTimeZone();
	
	@Test
	void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}
	
	@Test
	void GetAccountBalanceByAccountIdTest_ShouldReturn200() {
		
		client
				  .get()
				  .uri("/accounts/{accountId}/balance",ACCOUNT_ID)
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isOk();
		
	}
	
	@Test
	void GetAccountBalanceByAccountIdTest_WhenInvalidAccountId_ShouldReturn400() {
		
		client
				  .get()
				  .uri("/accounts/{accountId}/balance",ACCOUNT_ID+"_WrongAccountId$")
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
		
	}
	
	@Test
	void GetAccountBalanceByAccountIdTest_WhenWrongAccountId_ShouldReturn403() {
		
		client
				  .get()
				  .uri("/accounts/{accountId}/balance",Integer.parseInt(ACCOUNT_ID)+199999)
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.FORBIDDEN);
		
	}
	
	@Test
	void GetTransactionByAccountIdTest_ShouldReturn200() {
		
		client
				  .get()
				  .uri(uriBuilder -> uriBuilder
						    .path("/accounts/{accountId}/transactions")
						    .queryParam("fromAccountingDate", FROM_ACCOUNTING_DATE)
						    .queryParam("toAccountingDate", TO_ACCOUNTING_DATE)
						    .build(ACCOUNT_ID))
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isOk();
		
	}
	
	@Test
	void GetTransactionByAccountIdTest_WhenWrongAccountId_ShouldReturn403() {
		
		client
				  .get()
				  .uri(uriBuilder -> uriBuilder
						    .path("/accounts/{accountId}/transactions")
						    .queryParam("fromAccountingDate", FROM_ACCOUNTING_DATE)
						    .queryParam("toAccountingDate", TO_ACCOUNTING_DATE)
						    .build(Integer.parseInt(ACCOUNT_ID)+199999))
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.FORBIDDEN);
		
	}
	
	@Test
	void GetTransactionByAccountIdTest_WhenInvalidAccountId_ShouldReturn400() {
		
		client
				  .get()
				  .uri(uriBuilder -> uriBuilder
						    .path("/accounts/{accountId}/transactions")
						    .queryParam("fromAccountingDate", FROM_ACCOUNTING_DATE)
						    .queryParam("toAccountingDate", TO_ACCOUNTING_DATE)
						    .build(ACCOUNT_ID+"_WrongAccountId$"))
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
		
	}
	
	@Test
	void GetTransactionByAccountIdTest_WhenMissingFromDate_ShouldReturn400() {
		
		client
				  .get()
				  .uri(uriBuilder -> uriBuilder
						    .path("/accounts/{accountId}/transactions")
						    // .queryParam("fromAccountingDate", FROM_ACCOUNTING_DATE)
						    .queryParam("toAccountingDate", TO_ACCOUNTING_DATE)
						    .build(ACCOUNT_ID+"_WrongAccountId$"))
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
		
	}
	
	@Test
	void GetTransactionByAccountIdTest_WhenMissingToDate_ShouldReturn400() {
		
		client
				  .get()
				  .uri(uriBuilder -> uriBuilder
						    .path("/accounts/{accountId}/transactions")
						     .queryParam("fromAccountingDate", FROM_ACCOUNTING_DATE)
//						    .queryParam("toAccountingDate", TO_ACCOUNTING_DATE)
						    .build(ACCOUNT_ID+"_WrongAccountId$"))
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
		
	}
	
	@Test
	void GetTransactionByAccountIdTest_WhenUnorderedDates_ShouldReturn400() {
		
		client
				  .get()
				  .uri(uriBuilder -> uriBuilder
						    .path("/accounts/{accountId}/transactions")
						     .queryParam("fromAccountingDate", TO_ACCOUNTING_DATE) // <--
						    .queryParam("toAccountingDate", FROM_ACCOUNTING_DATE) // <--
						    .build(ACCOUNT_ID+"_WrongAccountId$"))
				  .accept(MediaType.APPLICATION_JSON)
					.exchange()
					.expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
		
	}
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Test
	void CreateMoneyTransferTest_ShouldReturn400() {
		
		String requestStr= """
				{
				  "creditor": {
				    "name": "John Doe",
				    "account": {
				      "accountCode": "IT60X0542811101000000123456",
				      "bicCode": "SELBIT2BXXX"
				    },
				    "address": {
				      "address": null,
				      "city": null,
				      "countryCode": "IT"
				    }
				  },
				  "executionDate": "2024-02-20",
				  "uri": "REMITTANCE_INFORMATION",
				  "description": "Payment invoice 75/2017",
				  "amount": 800,
				  "currency": "EUR",
				  "isUrgent": false,
				  "isInstant": false,
				  "feeType": "SHA",
				  "feeAccountId": "14537780",
				  "taxRelief": {
				    "taxReliefId": "L449",
				    "isCondoUpgrade": false,
				    "creditorFiscalCode": "PCCZZZ77M01D773K",
				    "beneficiaryType": "NATURAL_PERSON",
				    "naturalPersonBeneficiary": {
				      "fiscalCode1": "PCCZZZ77M01D773K",
				      "fiscalCode2": null,
				      "fiscalCode3": null,
				      "fiscalCode4": null,
				      "fiscalCode5": null
				    },
				    "legalPersonBeneficiary": {
				      "fiscalCode": null,
				      "legalRepresentativeFiscalCode": "PCCMCL77M01D773K"
				    }
				  }
				}
				""";
		
		try {
			
			MoneyTransferRequest requestBody= objectMapper.readValue(requestStr,MoneyTransferRequest.class);
			
			client
			  .post()
			  .uri(uriBuilder -> uriBuilder
					    .path("/accounts/{accountId}/payments/money-transfers")
					    .build(ACCOUNT_ID))
			  .headers(httpHeaders -> {
				    httpHeaders.set("X-Time-Zone",tz.getID());
			   })
			  .body(BodyInserters.fromValue(requestBody))
			  .accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
			
		} catch (JsonProcessingException e) {
			assertTrue(false,"Something went wrong while unmarshalling");
		}
		
	}
	
	
}
