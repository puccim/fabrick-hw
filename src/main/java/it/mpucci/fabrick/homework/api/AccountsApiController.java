package it.mpucci.fabrick.homework.api;

import it.mpucci.fabrick.homework.model.AccountBalance;
import it.mpucci.fabrick.homework.model.MoneyTransferRequest;
import it.mpucci.fabrick.homework.model.MoneyTransferResponse;
import it.mpucci.fabrick.homework.model.TransactionsList;
import it.mpucci.fabrick.homework.service.AccountService;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-11T22:02:43.266886963+01:00[Europe/Rome]")
@RestController
public class AccountsApiController implements AccountsApi {

    private static final Logger log = LoggerFactory.getLogger(AccountsApiController.class);

    private final HttpServletRequest request;
    
    private final AccountService accountService;

    public AccountsApiController(HttpServletRequest request, AccountService accountService) {
        this.request = request;
        this.accountService = accountService;
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    public ResponseEntity<MoneyTransferResponse> createMoneyTransfers(@Parameter(in = ParameterIn.HEADER, description = "" ,required=true,schema=@Schema()) @RequestHeader(value="X-Time-Zone", required=true) String xTimeZone,@Parameter(in = ParameterIn.PATH, description = "Account ID", required=true, schema=@Schema()) @PathVariable("accountId") Long accountId,@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody MoneyTransferRequest body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
            	
            	System.out.println(body);
            	
                return new ResponseEntity<MoneyTransferResponse>(accountService.createMoneyTransfers(xTimeZone, accountId, body), HttpStatus.OK);
                
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<MoneyTransferResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<MoneyTransferResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<AccountBalance> getCashBalanceByAccountId(@Parameter(in = ParameterIn.PATH, description = "Account ID", required=true, schema=@Schema()) @PathVariable("accountId") Long accountId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<AccountBalance>(accountService.getCashBalanceByAccountId(accountId), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<AccountBalance>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<AccountBalance>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<TransactionsList> getTransactionByAccountId(@Parameter(in = ParameterIn.PATH, description = "The ID of the account.", required=true, schema=@Schema()) @PathVariable("accountId") Long accountId,@NotNull @Pattern(regexp="^\\d{4}-\\d{2}-\\d{2}$") @Parameter(in = ParameterIn.QUERY, description = "The accounting date from which transactions should be fetched." ,required=true,schema=@Schema()) @Valid @RequestParam(value = "fromAccountingDate", required = true) String fromAccountingDate,@NotNull @Pattern(regexp="^\\d{4}-\\d{2}-\\d{2}$") @Parameter(in = ParameterIn.QUERY, description = "The accounting date to which transactions should be fetched." ,required=true,schema=@Schema()) @Valid @RequestParam(value = "toAccountingDate", required = true) String toAccountingDate) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<TransactionsList>(accountService.getTransactionByAccountId(accountId, fromAccountingDate, toAccountingDate), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<TransactionsList>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TransactionsList>(HttpStatus.NOT_IMPLEMENTED);
    }

}
