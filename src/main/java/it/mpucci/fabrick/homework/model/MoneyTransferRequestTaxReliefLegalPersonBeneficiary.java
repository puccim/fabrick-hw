package it.mpucci.fabrick.homework.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MoneyTransferRequestTaxReliefLegalPersonBeneficiary
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-12T22:25:19.601520846+01:00[Europe/Rome]")


public class MoneyTransferRequestTaxReliefLegalPersonBeneficiary  implements Serializable  {
  private static final long serialVersionUID = 1L;

  @JsonProperty("fiscalCode")
  private String fiscalCode = null;

  @JsonProperty("legalRepresentativeFiscalCode")
  private String legalRepresentativeFiscalCode = null;

  public MoneyTransferRequestTaxReliefLegalPersonBeneficiary fiscalCode(String fiscalCode) {
    this.fiscalCode = fiscalCode;
    return this;
  }

  /**
   * Get fiscalCode
   * @return fiscalCode
   **/
  @Schema(description = "")
  
    public String getFiscalCode() {
    return fiscalCode;
  }

  public void setFiscalCode(String fiscalCode) {
    this.fiscalCode = fiscalCode;
  }

  public MoneyTransferRequestTaxReliefLegalPersonBeneficiary legalRepresentativeFiscalCode(String legalRepresentativeFiscalCode) {
    this.legalRepresentativeFiscalCode = legalRepresentativeFiscalCode;
    return this;
  }

  /**
   * Get legalRepresentativeFiscalCode
   * @return legalRepresentativeFiscalCode
   **/
  @Schema(description = "")
  
    public String getLegalRepresentativeFiscalCode() {
    return legalRepresentativeFiscalCode;
  }

  public void setLegalRepresentativeFiscalCode(String legalRepresentativeFiscalCode) {
    this.legalRepresentativeFiscalCode = legalRepresentativeFiscalCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoneyTransferRequestTaxReliefLegalPersonBeneficiary moneyTransferRequestTaxReliefLegalPersonBeneficiary = (MoneyTransferRequestTaxReliefLegalPersonBeneficiary) o;
    return Objects.equals(this.fiscalCode, moneyTransferRequestTaxReliefLegalPersonBeneficiary.fiscalCode) &&
        Objects.equals(this.legalRepresentativeFiscalCode, moneyTransferRequestTaxReliefLegalPersonBeneficiary.legalRepresentativeFiscalCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fiscalCode, legalRepresentativeFiscalCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MoneyTransferRequestTaxReliefLegalPersonBeneficiary {\n");
    
    sb.append("    fiscalCode: ").append(toIndentedString(fiscalCode)).append("\n");
    sb.append("    legalRepresentativeFiscalCode: ").append(toIndentedString(legalRepresentativeFiscalCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
