package it.mpucci.fabrick.homework.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import it.mpucci.fabrick.homework.model.MoneyTransferRequestTaxReliefLegalPersonBeneficiary;
import it.mpucci.fabrick.homework.model.MoneyTransferRequestTaxReliefNaturalPersonBeneficiary;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MoneyTransferRequestTaxRelief
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-12T22:25:19.601520846+01:00[Europe/Rome]")


public class MoneyTransferRequestTaxRelief  implements Serializable  {
  private static final long serialVersionUID = 1L;

  @JsonProperty("taxReliefId")
  private String taxReliefId = null;

  @JsonProperty("isCondoUpgrade")
  private Boolean isCondoUpgrade = null;

  @JsonProperty("creditorFiscalCode")
  private String creditorFiscalCode = null;

  @JsonProperty("beneficiaryType")
  private String beneficiaryType = null;

  @JsonProperty("naturalPersonBeneficiary")
  private MoneyTransferRequestTaxReliefNaturalPersonBeneficiary naturalPersonBeneficiary = null;

  @JsonProperty("legalPersonBeneficiary")
  private MoneyTransferRequestTaxReliefLegalPersonBeneficiary legalPersonBeneficiary = null;

  public MoneyTransferRequestTaxRelief taxReliefId(String taxReliefId) {
    this.taxReliefId = taxReliefId;
    return this;
  }

  /**
   * Get taxReliefId
   * @return taxReliefId
   **/
  @Schema(description = "")
  
    public String getTaxReliefId() {
    return taxReliefId;
  }

  public void setTaxReliefId(String taxReliefId) {
    this.taxReliefId = taxReliefId;
  }

  public MoneyTransferRequestTaxRelief isCondoUpgrade(Boolean isCondoUpgrade) {
    this.isCondoUpgrade = isCondoUpgrade;
    return this;
  }

  /**
   * Get isCondoUpgrade
   * @return isCondoUpgrade
   **/
  @Schema(description = "")
  
    public Boolean isIsCondoUpgrade() {
    return isCondoUpgrade;
  }

  public void setIsCondoUpgrade(Boolean isCondoUpgrade) {
    this.isCondoUpgrade = isCondoUpgrade;
  }

  public MoneyTransferRequestTaxRelief creditorFiscalCode(String creditorFiscalCode) {
    this.creditorFiscalCode = creditorFiscalCode;
    return this;
  }

  /**
   * Get creditorFiscalCode
   * @return creditorFiscalCode
   **/
  @Schema(description = "")
  
    public String getCreditorFiscalCode() {
    return creditorFiscalCode;
  }

  public void setCreditorFiscalCode(String creditorFiscalCode) {
    this.creditorFiscalCode = creditorFiscalCode;
  }

  public MoneyTransferRequestTaxRelief beneficiaryType(String beneficiaryType) {
    this.beneficiaryType = beneficiaryType;
    return this;
  }

  /**
   * Get beneficiaryType
   * @return beneficiaryType
   **/
  @Schema(description = "")
  
    public String getBeneficiaryType() {
    return beneficiaryType;
  }

  public void setBeneficiaryType(String beneficiaryType) {
    this.beneficiaryType = beneficiaryType;
  }

  public MoneyTransferRequestTaxRelief naturalPersonBeneficiary(MoneyTransferRequestTaxReliefNaturalPersonBeneficiary naturalPersonBeneficiary) {
    this.naturalPersonBeneficiary = naturalPersonBeneficiary;
    return this;
  }

  /**
   * Get naturalPersonBeneficiary
   * @return naturalPersonBeneficiary
   **/
  @Schema(description = "")
  
    @Valid
    public MoneyTransferRequestTaxReliefNaturalPersonBeneficiary getNaturalPersonBeneficiary() {
    return naturalPersonBeneficiary;
  }

  public void setNaturalPersonBeneficiary(MoneyTransferRequestTaxReliefNaturalPersonBeneficiary naturalPersonBeneficiary) {
    this.naturalPersonBeneficiary = naturalPersonBeneficiary;
  }

  public MoneyTransferRequestTaxRelief legalPersonBeneficiary(MoneyTransferRequestTaxReliefLegalPersonBeneficiary legalPersonBeneficiary) {
    this.legalPersonBeneficiary = legalPersonBeneficiary;
    return this;
  }

  /**
   * Get legalPersonBeneficiary
   * @return legalPersonBeneficiary
   **/
  @Schema(description = "")
  
    @Valid
    public MoneyTransferRequestTaxReliefLegalPersonBeneficiary getLegalPersonBeneficiary() {
    return legalPersonBeneficiary;
  }

  public void setLegalPersonBeneficiary(MoneyTransferRequestTaxReliefLegalPersonBeneficiary legalPersonBeneficiary) {
    this.legalPersonBeneficiary = legalPersonBeneficiary;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoneyTransferRequestTaxRelief moneyTransferRequestTaxRelief = (MoneyTransferRequestTaxRelief) o;
    return Objects.equals(this.taxReliefId, moneyTransferRequestTaxRelief.taxReliefId) &&
        Objects.equals(this.isCondoUpgrade, moneyTransferRequestTaxRelief.isCondoUpgrade) &&
        Objects.equals(this.creditorFiscalCode, moneyTransferRequestTaxRelief.creditorFiscalCode) &&
        Objects.equals(this.beneficiaryType, moneyTransferRequestTaxRelief.beneficiaryType) &&
        Objects.equals(this.naturalPersonBeneficiary, moneyTransferRequestTaxRelief.naturalPersonBeneficiary) &&
        Objects.equals(this.legalPersonBeneficiary, moneyTransferRequestTaxRelief.legalPersonBeneficiary);
  }

  @Override
  public int hashCode() {
    return Objects.hash(taxReliefId, isCondoUpgrade, creditorFiscalCode, beneficiaryType, naturalPersonBeneficiary, legalPersonBeneficiary);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MoneyTransferRequestTaxRelief {\n");
    
    sb.append("    taxReliefId: ").append(toIndentedString(taxReliefId)).append("\n");
    sb.append("    isCondoUpgrade: ").append(toIndentedString(isCondoUpgrade)).append("\n");
    sb.append("    creditorFiscalCode: ").append(toIndentedString(creditorFiscalCode)).append("\n");
    sb.append("    beneficiaryType: ").append(toIndentedString(beneficiaryType)).append("\n");
    sb.append("    naturalPersonBeneficiary: ").append(toIndentedString(naturalPersonBeneficiary)).append("\n");
    sb.append("    legalPersonBeneficiary: ").append(toIndentedString(legalPersonBeneficiary)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
