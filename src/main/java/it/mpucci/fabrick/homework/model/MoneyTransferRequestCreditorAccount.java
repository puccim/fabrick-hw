package it.mpucci.fabrick.homework.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MoneyTransferRequestCreditorAccount
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-12T22:25:19.601520846+01:00[Europe/Rome]")


public class MoneyTransferRequestCreditorAccount  implements Serializable  {
  private static final long serialVersionUID = 1L;

  @JsonProperty("accountCode")
  private String accountCode = null;

  @JsonProperty("bicCode")
  private String bicCode = null;

  public MoneyTransferRequestCreditorAccount accountCode(String accountCode) {
    this.accountCode = accountCode;
    return this;
  }

  /**
   * Get accountCode
   * @return accountCode
   **/
  @Schema(description = "")
  
    public String getAccountCode() {
    return accountCode;
  }

  public void setAccountCode(String accountCode) {
    this.accountCode = accountCode;
  }

  public MoneyTransferRequestCreditorAccount bicCode(String bicCode) {
    this.bicCode = bicCode;
    return this;
  }

  /**
   * Get bicCode
   * @return bicCode
   **/
  @Schema(description = "")
  
    public String getBicCode() {
    return bicCode;
  }

  public void setBicCode(String bicCode) {
    this.bicCode = bicCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoneyTransferRequestCreditorAccount moneyTransferRequestCreditorAccount = (MoneyTransferRequestCreditorAccount) o;
    return Objects.equals(this.accountCode, moneyTransferRequestCreditorAccount.accountCode) &&
        Objects.equals(this.bicCode, moneyTransferRequestCreditorAccount.bicCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountCode, bicCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MoneyTransferRequestCreditorAccount {\n");
    
    sb.append("    accountCode: ").append(toIndentedString(accountCode)).append("\n");
    sb.append("    bicCode: ").append(toIndentedString(bicCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
