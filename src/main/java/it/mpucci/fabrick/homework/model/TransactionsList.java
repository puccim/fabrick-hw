package it.mpucci.fabrick.homework.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import it.mpucci.fabrick.homework.model.Transaction;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TransactionsList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-14T21:55:34.150287747+01:00[Europe/Rome]")


public class TransactionsList  implements Serializable  {
  private static final long serialVersionUID = 1L;

  @JsonProperty("list")
  @Valid
  private List<Transaction> list = null;

  public TransactionsList list(List<Transaction> list) {
    this.list = list;
    return this;
  }

  public TransactionsList addListItem(Transaction listItem) {
    if (this.list == null) {
      this.list = new ArrayList<Transaction>();
    }
    this.list.add(listItem);
    return this;
  }

  /**
   * Get list
   * @return list
   **/
  @Schema(description = "")
      @Valid
    public List<Transaction> getList() {
    return list;
  }

  public void setList(List<Transaction> list) {
    this.list = list;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionsList transactionsList = (TransactionsList) o;
    return Objects.equals(this.list, transactionsList.list);
  }

  @Override
  public int hashCode() {
    return Objects.hash(list);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionsList {\n");
    
    sb.append("    list: ").append(toIndentedString(list)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
