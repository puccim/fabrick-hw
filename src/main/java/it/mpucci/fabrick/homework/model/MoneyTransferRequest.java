package it.mpucci.fabrick.homework.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import it.mpucci.fabrick.homework.model.CurrencyEnum;
import it.mpucci.fabrick.homework.model.MoneyTransferRequestCreditor;
import it.mpucci.fabrick.homework.model.MoneyTransferRequestTaxRelief;
import java.util.Date;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MoneyTransferRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-12T22:25:19.601520846+01:00[Europe/Rome]")


public class MoneyTransferRequest  implements Serializable  {
  private static final long serialVersionUID = 1L;

  @JsonProperty("creditor")
  private MoneyTransferRequestCreditor creditor = null;

  @JsonProperty("executionDate")
  private String executionDate = null;

  @JsonProperty("uri")
  private String uri = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("amount")
  private Float amount = null;

  @JsonProperty("currency")
  private CurrencyEnum currency = null;

  @JsonProperty("isUrgent")
  private Boolean isUrgent = null;

  @JsonProperty("isInstant")
  private Boolean isInstant = null;

  @JsonProperty("feeType")
  private String feeType = null;

  @JsonProperty("feeAccountId")
  private String feeAccountId = null;

  @JsonProperty("taxRelief")
  private MoneyTransferRequestTaxRelief taxRelief = null;

  public MoneyTransferRequest creditor(MoneyTransferRequestCreditor creditor) {
    this.creditor = creditor;
    return this;
  }

  /**
   * Get creditor
   * @return creditor
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public MoneyTransferRequestCreditor getCreditor() {
    return creditor;
  }

  public void setCreditor(MoneyTransferRequestCreditor creditor) {
    this.creditor = creditor;
  }

  public MoneyTransferRequest executionDate(String executionDate) {
    this.executionDate = executionDate;
    return this;
  }

  /**
   * Get executionDate
   * @return executionDate
   **/
  @Schema(description = "")
  
    @Valid
    public String getExecutionDate() {
    return executionDate;
  }

  public void setExecutionDate(String executionDate) {
    this.executionDate = executionDate;
  }

  public MoneyTransferRequest uri(String uri) {
    this.uri = uri;
    return this;
  }

  /**
   * Get uri
   * @return uri
   **/
  @Schema(description = "")
  
    public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public MoneyTransferRequest description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public MoneyTransferRequest amount(Float amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
   **/
  @Schema(required = true, description = "")
      @NotNull

    public Float getAmount() {
    return amount;
  }

  public void setAmount(Float amount) {
    this.amount = amount;
  }

  public MoneyTransferRequest currency(CurrencyEnum currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public CurrencyEnum getCurrency() {
    return currency;
  }

  public void setCurrency(CurrencyEnum currency) {
    this.currency = currency;
  }

  public MoneyTransferRequest isUrgent(Boolean isUrgent) {
    this.isUrgent = isUrgent;
    return this;
  }

  /**
   * Get isUrgent
   * @return isUrgent
   **/
  @Schema(description = "")
  
    public Boolean isIsUrgent() {
    return isUrgent;
  }

  public void setIsUrgent(Boolean isUrgent) {
    this.isUrgent = isUrgent;
  }

  public MoneyTransferRequest isInstant(Boolean isInstant) {
    this.isInstant = isInstant;
    return this;
  }

  /**
   * Get isInstant
   * @return isInstant
   **/
  @Schema(description = "")
  
    public Boolean isIsInstant() {
    return isInstant;
  }

  public void setIsInstant(Boolean isInstant) {
    this.isInstant = isInstant;
  }

  public MoneyTransferRequest feeType(String feeType) {
    this.feeType = feeType;
    return this;
  }

  /**
   * Get feeType
   * @return feeType
   **/
  @Schema(description = "")
  
    public String getFeeType() {
    return feeType;
  }

  public void setFeeType(String feeType) {
    this.feeType = feeType;
  }

  public MoneyTransferRequest feeAccountId(String feeAccountId) {
    this.feeAccountId = feeAccountId;
    return this;
  }

  /**
   * Get feeAccountId
   * @return feeAccountId
   **/
  @Schema(description = "")
  
    public String getFeeAccountId() {
    return feeAccountId;
  }

  public void setFeeAccountId(String feeAccountId) {
    this.feeAccountId = feeAccountId;
  }

  public MoneyTransferRequest taxRelief(MoneyTransferRequestTaxRelief taxRelief) {
    this.taxRelief = taxRelief;
    return this;
  }

  /**
   * Get taxRelief
   * @return taxRelief
   **/
  @Schema(description = "")
  
    @Valid
    public MoneyTransferRequestTaxRelief getTaxRelief() {
    return taxRelief;
  }

  public void setTaxRelief(MoneyTransferRequestTaxRelief taxRelief) {
    this.taxRelief = taxRelief;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoneyTransferRequest moneyTransferRequest = (MoneyTransferRequest) o;
    return Objects.equals(this.creditor, moneyTransferRequest.creditor) &&
        Objects.equals(this.executionDate, moneyTransferRequest.executionDate) &&
        Objects.equals(this.uri, moneyTransferRequest.uri) &&
        Objects.equals(this.description, moneyTransferRequest.description) &&
        Objects.equals(this.amount, moneyTransferRequest.amount) &&
        Objects.equals(this.currency, moneyTransferRequest.currency) &&
        Objects.equals(this.isUrgent, moneyTransferRequest.isUrgent) &&
        Objects.equals(this.isInstant, moneyTransferRequest.isInstant) &&
        Objects.equals(this.feeType, moneyTransferRequest.feeType) &&
        Objects.equals(this.feeAccountId, moneyTransferRequest.feeAccountId) &&
        Objects.equals(this.taxRelief, moneyTransferRequest.taxRelief);
  }

  @Override
  public int hashCode() {
    return Objects.hash(creditor, executionDate, uri, description, amount, currency, isUrgent, isInstant, feeType, feeAccountId, taxRelief);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MoneyTransferRequest {\n");
    
    sb.append("    creditor: ").append(toIndentedString(creditor)).append("\n");
    sb.append("    executionDate: ").append(toIndentedString(executionDate)).append("\n");
    sb.append("    uri: ").append(toIndentedString(uri)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    isUrgent: ").append(toIndentedString(isUrgent)).append("\n");
    sb.append("    isInstant: ").append(toIndentedString(isInstant)).append("\n");
    sb.append("    feeType: ").append(toIndentedString(feeType)).append("\n");
    sb.append("    feeAccountId: ").append(toIndentedString(feeAccountId)).append("\n");
    sb.append("    taxRelief: ").append(toIndentedString(taxRelief)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
