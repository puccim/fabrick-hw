package it.mpucci.fabrick.homework.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import it.mpucci.fabrick.homework.model.MoneyTransferRequestCreditorAccount;
import it.mpucci.fabrick.homework.model.MoneyTransferRequestCreditorAddress;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MoneyTransferRequestCreditor
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2024-02-12T22:25:19.601520846+01:00[Europe/Rome]")


public class MoneyTransferRequestCreditor  implements Serializable  {
  private static final long serialVersionUID = 1L;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("account")
  private MoneyTransferRequestCreditorAccount account = null;

  @JsonProperty("address")
  private MoneyTransferRequestCreditorAddress address = null;

  public MoneyTransferRequestCreditor name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(description = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public MoneyTransferRequestCreditor account(MoneyTransferRequestCreditorAccount account) {
    this.account = account;
    return this;
  }

  /**
   * Get account
   * @return account
   **/
  @Schema(description = "")
  
    @Valid
    public MoneyTransferRequestCreditorAccount getAccount() {
    return account;
  }

  public void setAccount(MoneyTransferRequestCreditorAccount account) {
    this.account = account;
  }

  public MoneyTransferRequestCreditor address(MoneyTransferRequestCreditorAddress address) {
    this.address = address;
    return this;
  }

  /**
   * Get address
   * @return address
   **/
  @Schema(description = "")
  
    @Valid
    public MoneyTransferRequestCreditorAddress getAddress() {
    return address;
  }

  public void setAddress(MoneyTransferRequestCreditorAddress address) {
    this.address = address;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoneyTransferRequestCreditor moneyTransferRequestCreditor = (MoneyTransferRequestCreditor) o;
    return Objects.equals(this.name, moneyTransferRequestCreditor.name) &&
        Objects.equals(this.account, moneyTransferRequestCreditor.account) &&
        Objects.equals(this.address, moneyTransferRequestCreditor.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, account, address);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MoneyTransferRequestCreditor {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
