package it.mpucci.fabrick.homework.service;

import java.io.IOException;
import java.time.Duration;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.handler.logging.LogLevel;
import it.mpucci.fabrick.homework.exception.MoneryTransferNeverWorkException;
import it.mpucci.fabrick.homework.model.AccountBalance;
import it.mpucci.fabrick.homework.model.MoneyTransferRequest;
import it.mpucci.fabrick.homework.model.MoneyTransferResponse;
import it.mpucci.fabrick.homework.model.TransactionsList;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

@Slf4j
@Service
public class AccountService implements IAccountService {
	
	@Value("${fabrick.api.base_url}")
	private String baseUrl;
	
	@Value("${fabrick.api.endpoint.lettura_saldo}")
	private String endpointLetturaSaldo;
	
	@Value("${fabrick.api.endpoint.money_transfer}")
	private String endpointMoneyTransfer;
	
	@Value("${fabrick.api.endpoint.elenco_transazioni}")
	private String endpointElencoTransazioni;
	
	@Value("${fabrick.api.api-key}")
	private String apiKey;
	
	@Value("${fabrick.api.auth-schema}")
	private String authSchema;
	
	public AccountService() {
	}
	
	private WebClient webClient;
	
	public record HttpAccountResponse(String status,List<Object> errors,AccountBalance payload) {}
	
	public record HttpTransactionsListResponse(String status,List<Object> errors,TransactionsList payload) {}
	
	private static TimeZone tz = Calendar.getInstance().getTimeZone();

	@PostConstruct
	void init() {
		
		var client = HttpClient.create()
				.wiretap(true)
				/*.wiretap("reactor.netty.http.client.HttpClient", 
					    LogLevel.DEBUG, AdvancedByteBufFormat.TEXTUAL)*/
				.responseTimeout(Duration.ofSeconds(60));
		
		this.webClient = WebClient.builder().clientConnector(new ReactorClientHttpConnector(client)).baseUrl(baseUrl)
				.defaultHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
				.defaultHeader("Api-Key", apiKey)
				.defaultHeader("Auth-Schema", authSchema)
//				.defaultHeader("X-Time-Zone",tz.getID())
				.build();
		
	}
  
	@Override
	public AccountBalance getCashBalanceByAccountId(Long accountId) throws IOException {
		
		AccountBalance result= null;
		
			/*
			 *  {
				  "status" : "KO",
				  "errors" : [{"code":"REQ016","description":"Invalid Header: Auth-Schema","params":""}],
				  "payload": {}
				}
			 */
		
		try {
			
			HttpAccountResponse response= webClient
					.get()
					.uri(endpointLetturaSaldo,accountId)
					.retrieve()
					.bodyToMono(HttpAccountResponse.class)
					.block();
			
			log.debug(response.toString());
			
			result= response.payload();

		} catch (RuntimeException e) {
			log.error(e.toString());
			throw e;
		}
		
		return result;
		
	}
	
	/**
	 * 	&quot;code&quot;: &quot;API000&quot;,
		&quot;description&quot;: &quot;Errore tecnico La condizione BP049 non e&#39; prevista per il conto id
		14537780&quot;
	 */
	@Override
	public MoneyTransferResponse createMoneyTransfers(String xTimeZone, Long accountId,MoneyTransferRequest body) throws IOException {
		
		MoneyTransferResponse result= null;
		
		try {
			
			String response= webClient.post()
					.uri(uriBuilder -> uriBuilder
							.path(endpointMoneyTransfer)
							.build(accountId))
					.body(BodyInserters.fromValue(body))
					.header("X-Time-Zone","Europe/London")
					.retrieve()
				    .onStatus(
				    		HttpStatus.INTERNAL_SERVER_ERROR::equals,
				    		clientResponse -> clientResponse.bodyToMono(String.class).map(MoneryTransferNeverWorkException::new))
				    .onStatus(
			    			HttpStatus.BAD_REQUEST::equals,
			    			clientResponse -> clientResponse.bodyToMono(String.class).map(MoneryTransferNeverWorkException::new))
				    .bodyToMono(String.class)
					.block();
			
		} catch (Exception e) {
			
			log.error(e.toString());
			log.error(e.getClass().toString());
			throw e;
			
		}
		
		return result;
		
	}
	
	@Override
	public TransactionsList getTransactionByAccountId(Long accountId,String fromAccountingDate,String toAccountingDate) throws IOException {
		
		TransactionsList result= null;
		
		try {
			
			HttpTransactionsListResponse response= webClient.get()
					.uri(uriBuilder -> uriBuilder
							.path(endpointElencoTransazioni)
							.queryParam("fromAccountingDate", fromAccountingDate)
							.queryParam("toAccountingDate", toAccountingDate)
							.build(accountId))
					.retrieve()
					.bodyToMono(HttpTransactionsListResponse.class)
					.block();
			
			result= response.payload();
			
			log.debug(result.toString());

		} catch (RuntimeException e) {
			log.error(e.toString());
			throw e;
		}
		
		return result;
		
	}
	
}
