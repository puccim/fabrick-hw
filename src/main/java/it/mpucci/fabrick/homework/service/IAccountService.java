package it.mpucci.fabrick.homework.service;

import java.io.IOException;

import it.mpucci.fabrick.homework.model.AccountBalance;
import it.mpucci.fabrick.homework.model.MoneyTransferRequest;
import it.mpucci.fabrick.homework.model.MoneyTransferResponse;
import it.mpucci.fabrick.homework.model.TransactionsList;

public interface IAccountService {

	MoneyTransferResponse createMoneyTransfers(String xTimeZone, Long accountId, MoneyTransferRequest body) throws IOException;

	AccountBalance getCashBalanceByAccountId(Long accountId) throws IOException;

	TransactionsList getTransactionByAccountId(Long accountId, String fromAccountingDate, String toAccountingDate) throws IOException;

}
