package it.mpucci.fabrick.homework.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

/**
 * Home redirection to swagger api documentation 
 */
@Slf4j
@Controller
public class HomeController {
    @RequestMapping(value = "/")
    public String index() {
        log.debug("/swagger-ui/index.html");
        return "redirect:/swagger-ui/";
    }
}
