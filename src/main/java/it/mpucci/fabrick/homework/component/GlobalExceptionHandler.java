package it.mpucci.fabrick.homework.component;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import it.mpucci.fabrick.homework.exception.MoneryTransferNeverWorkException;

import it.mpucci.fabrick.homework.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public final class GlobalExceptionHandler {

	@ExceptionHandler(WebClientResponseException.class)
	public ResponseEntity<ErrorResponse> handleProductNotFoundException(WebClientResponseException ex) {
		
		log.error("Catching {}",ex.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(ex.getStatusCode().value(), ex.getMessage());
		return ResponseEntity.status(ex.getStatusCode().value()).body(errorResponse);
		
	}
	
	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<ErrorResponse> handleNumberFormatException(NumberFormatException ex) {
		
		log.error("Catching {}",ex.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
		
	}
	
	@ExceptionHandler(MissingRequestHeaderException.class)
	public ResponseEntity<ErrorResponse> handleMissingRequestHeaderException(MissingRequestHeaderException ex) {
		
		log.error("Catching {}",ex.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
		
	}
	
	@ExceptionHandler(MoneryTransferNeverWorkException.class)
	public ResponseEntity<ErrorResponse> MoneryTransferNeverWorkException(MoneryTransferNeverWorkException ex) {
		
		log.error("Catching {}",ex.getMessage());
		ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
		
	}
	
	
	
}