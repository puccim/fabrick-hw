package it.mpucci.fabrick.homework.exception;

public class MoneryTransferNeverWorkException extends RuntimeException {

	public MoneryTransferNeverWorkException() {
		super();
	}

	public MoneryTransferNeverWorkException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MoneryTransferNeverWorkException(String message, Throwable cause) {
		super(message, cause);
	}

	public MoneryTransferNeverWorkException(String message) {
		super(message);
	}

	public MoneryTransferNeverWorkException(Throwable cause) {
		super(cause);
	}

}
