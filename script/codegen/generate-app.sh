#!/bin/bash
#
# java -jar swagger-codegen-cli.jar  config-help -l spring
#

APP_NAME=FabrickHomework

CODEGEN_DIR="$(dirname "$0")/."
CODEGEN_JAR=${CODEGEN_DIR}/swagger-codegen-cli.jar
CODEGEN_JAR_VERSION="3.0.41"
CODEGEN_SWAGGER=${CODEGEN_DIR}/api_v1.yaml
CODEGEN_CONFIG=${CODEGEN_DIR}/swagger-codegen-spring-options.json
CODEGEN_OUT_DIR=${CODEGEN_DIR}/build/${APP_NAME}

# delete build folder
rm -rf ${CODEGEN_OUT_DIR}
mkdir -p ${CODEGEN_OUT_DIR}

# Download step
#rm -f ${CODEGEN_JAR} ;
#wget --no-check-certificate https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/${CODEGEN_JAR_VERSION}/swagger-codegen-cli-${CODEGEN_JAR_VERSION}.jar -O ${CODEGEN_JAR}

java -jar ${CODEGEN_JAR} generate \
    -i ${CODEGEN_SWAGGER} \
    -l spring \
    -o ${CODEGEN_OUT_DIR} \
    -c ${CODEGEN_CONFIG} \
    -DhideGenerationTimestamp=true \
	-DserializableModel=true

exit 0
